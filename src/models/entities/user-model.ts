import { AccountModel } from './account-model';
import { PerfilModel } from './perfil-model';
import { BiometricData } from './biometric-data-model';
import { ValueMasterModel } from 'models/entities/value-master';
export class UserModel {
    id: number;
    nombres: string;
    apellidos: string;
    documento: string;
    email: string;
    telefono: string;
    contrasena: string;
    direccion: string;
    fechaNacimiento: Date;
    tipoDocumento: ValueMasterModel;
    tipoSangre: ValueMasterModel;
    idDatoBiometrico: BiometricData;
    idPerfil: PerfilModel;
    idCuenta: AccountModel;

    constructor() {
        this.id = null;
        this.nombres = '';
        this.apellidos = '';
        this.documento = '';
        this.email = '';
        this.telefono = '';
        this.contrasena = '';
        this.direccion = '';
        this.fechaNacimiento = new Date();
        this.tipoDocumento = new ValueMasterModel();
        this.tipoSangre = new ValueMasterModel();
        this.idDatoBiometrico = new BiometricData();
        this.idPerfil = new PerfilModel();
        this.idCuenta = new AccountModel();
    }

}