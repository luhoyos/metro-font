import { ValueMasterModel } from './value-master';

export class PerfilModel {
    id: number;
    nombre: string;
    descripcion: string;
    tipoPerfil: ValueMasterModel;

    constructor() {
        this.id = null;
        this.nombre = '';
        this.descripcion = '';
        this.tipoPerfil = new ValueMasterModel();
    }
}