import { RatingModel } from './rating-model';

export class BenefitModel {
    id: number;
    nombre: string;
    descripcion: string;
    idTarifa: {
        id: number;
    };

    constructor() {
        this.id = null;
        this.nombre = '';
        this.descripcion = '';
        this.idTarifa = new RatingModel();
    }
}
