import { ValueMasterModel } from './value-master';

export class VehicleModel{
    id: number;
    capacidad: number;
    nombre: '';
    idTipoVehiculo: {
        id: number;
    };

    constructor() {
        this.id = null;
        this.capacidad = null;
        this.nombre = '';
        this.idTipoVehiculo = new ValueMasterModel();
    }
}
