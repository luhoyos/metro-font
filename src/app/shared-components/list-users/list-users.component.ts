import { FilterUsersModel } from './../../../models/searching/filter-users-model';
import { GenericResponseModel } from './../../../models/response/generic-response-model';
import { user } from './../../../constants/api-constants';
import { BackendServiceService } from './../../shared/backend-service/backend-service.service';
import { UserModel } from './../../../models/entities/user-model';
import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.scss']
})
export class ListUsersComponent implements OnInit {

  users: Array<UserModel> = new Array();
  @ViewChild('buttonsTemplate', null) buttonsTemplate: TemplateRef<any>;
  @ViewChild('dateTemplate', null) dateTemplate: TemplateRef<any>;

  columns;

  currentPage = 0;
  totalElements = 0;
  filterInfoLocal: FilterUsersModel;
  isSearching = false;

  constructor(
    private backendServices: BackendServiceService
  ) { }

  ngOnInit() {
    this.getUsers();
    this.columns = [
      { prop: 'id', name: 'Identificador', width: 100 },
      { prop: 'nombres', name: 'Nombres', width: 100 },
      { prop: 'apellidos', name: 'Apellidos', width: 100 },
      { prop: 'documento', name: 'Documento', width: 100 },
      { prop: 'email', name: 'Email', width: 100 },
      { prop: 'telefono', name: 'Telefono', width: 100 },
      { prop: 'direccion', name: 'Direccion', width: 100 },
      { prop: 'fechaNacimiento', name: 'Fecha Nacimiento', width: 100, cellTemplate: this.dateTemplate },
      { prop: 'tipoDocumento.dato', name: 'Tipo Doccumento', width: 300 },
      { prop: 'opciones', name: 'Opciones', cellTemplate: this.buttonsTemplate }
    ]
  }

  setPage(pageRequest) {
    this.currentPage = pageRequest.offset;
    if (this.isSearching) {
      setTimeout(() => {
        this.listWithFilterLocal();
      }, 1000);
    } else {
      setTimeout(() => {
        this.getUsers();
      }, 1000);
    }
  }

  getUsers() {
    this.backendServices.getService(user.apiGet, {
      pageRequest: this.currentPage
    }).then(
      (getUsersResponse: GenericResponseModel) => {
        this.users = getUsersResponse.listaRespuesta;
        this.totalElements = Number(getUsersResponse.variable);
      }
    ).catch(
      (getUsersError) => {
        console.error(getUsersError);
      }
    )
  }

  public listWithFilter(filterInfo: FilterUsersModel) {
    this.filterInfoLocal = { ...filterInfo }
    this.isSearching = true;
    this.backendServices.getService(user.apiGetFilter, {
      userName: filterInfo.userName,
      userDocument: filterInfo.userDocument,
      pageRequest: this.currentPage
    }).then(
      (getUsersResponse: GenericResponseModel) => {
        console.log(getUsersResponse);
        this.users = getUsersResponse.listaRespuesta;
      }
    ).catch(
      (getUsersError) => {
        console.error(getUsersError);
      }
    )
  }

  public listWithFilterLocal() {
    debugger;
    this.backendServices.getService(user.apiGetFilter, {
      userName: this.filterInfoLocal.userName,
      userDocument: this.filterInfoLocal.userDocument,
      pageRequest: this.currentPage
    }).then(
      (getUsersResponse: GenericResponseModel) => {
        console.log(getUsersResponse);
        this.users = getUsersResponse.listaRespuesta;
      }
    ).catch(
      (getUsersError) => {
        console.error(getUsersError);
      }
    )
  }

}
