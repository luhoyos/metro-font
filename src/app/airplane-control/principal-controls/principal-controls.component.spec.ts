import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrincipalControlsComponent } from './principal-controls.component';

describe('PrincipalControlsComponent', () => {
  let component: PrincipalControlsComponent;
  let fixture: ComponentFixture<PrincipalControlsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrincipalControlsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrincipalControlsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
