import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-principal-controls',
  templateUrl: './principal-controls.component.html',
  styleUrls: ['./principal-controls.component.scss']
})
export class PrincipalControlsComponent implements OnInit {

  @Output() eventEmitter: EventEmitter<any> = new EventEmitter();
  canTakeOff = false;
  canToLoan = false;
  canViewReport = false;

  constructor() { }

  ngOnInit() {
  }

  sendNewDirective(newDirective) {
    this.eventEmitter.emit(newDirective);
  }

}
