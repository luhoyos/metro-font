import { PrincipalControlComponent } from './principal-control/principal-control.component';
import { AirplaneViewComponent } from './airplane-view/airplane-view.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'ver-avion',
        component: AirplaneViewComponent
      },
      {
        path: 'ver-control',
        component: PrincipalControlComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AirplaneControlRoutingModule { }
