import { FormsModule } from '@angular/forms';
import { PrincipalControlComponent } from './principal-control/principal-control.component';
import { AirplaneViewComponent } from './airplane-view/airplane-view.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AirplaneControlRoutingModule } from './airplane-control-routing.module';
import { PrincipalControlsComponent } from './principal-controls/principal-controls.component';
import { SecondaryControlsComponent } from './secondary-controls/secondary-controls.component';

@NgModule({
  declarations: [PrincipalControlsComponent, SecondaryControlsComponent, AirplaneViewComponent, PrincipalControlComponent],
  imports: [
    CommonModule,
    AirplaneControlRoutingModule,
    FormsModule
  ]
})
export class AirplaneControlModule { }
