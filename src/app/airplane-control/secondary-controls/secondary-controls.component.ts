import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-secondary-controls',
  templateUrl: './secondary-controls.component.html',
  styleUrls: ['./secondary-controls.component.scss']
})
export class SecondaryControlsComponent implements OnInit {

  userAlt = 0;
  @Output() eventEmitter: EventEmitter<any> = new EventEmitter();
  isFlying = false;

  constructor() { }

  ngOnInit() {
  }

  sendNewDirective(newDirective) {
    this.eventEmitter.emit(newDirective);
  }

}
