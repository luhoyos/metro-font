import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecondaryControlsComponent } from './secondary-controls.component';

describe('SecondaryControlsComponent', () => {
  let component: SecondaryControlsComponent;
  let fixture: ComponentFixture<SecondaryControlsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecondaryControlsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondaryControlsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
