import { PrincipalControlsComponent } from './../principal-controls/principal-controls.component';
import { SecondaryControlsComponent } from './../secondary-controls/secondary-controls.component';
import { AirplaneViewComponent } from './../airplane-view/airplane-view.component';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-principal-control',
  templateUrl: './principal-control.component.html',
  styleUrls: ['./principal-control.component.scss']
})
export class PrincipalControlComponent implements OnInit {

  @ViewChild(AirplaneViewComponent, null) airplaneComponent: AirplaneViewComponent;
  @ViewChild(SecondaryControlsComponent, null) secondaryControls: SecondaryControlsComponent;
  @ViewChild(PrincipalControlsComponent, null) principalComponent: PrincipalControlsComponent;

  

  constructor() { }

  ngOnInit() {
    this.validateTakeOff();
    this.validateToLand();
    this.isInHome();
  }

  getNewPrincipalDirective(newPrincipalDirective) {
    switch (newPrincipalDirective) {
      case 'S': {
        this.airplaneComponent.moveAirplaneHorizontally();
        break;
      }
      case 'T': {
        this.secondaryControls.isFlying = true;
        break;
      }
      case 'L': {
        this.airplaneComponent.toLand();
        break;
      }
    }
  }

  getNewSecondaryDirective(newSecondaryDirective) {
    switch (newSecondaryDirective) {
      case 'DOWN': {
        this.airplaneComponent.downAirplane();
        break;
      }
      case 'UP': {
        this.airplaneComponent.upAirplane();
        break;
      }
      case 'TO': {
        this.airplaneComponent.toAirplane(this.secondaryControls.userAlt);
        break;
      }
      case 'EJECT': {
        this.airplaneComponent.peopleFalling.push([this.airplaneComponent.airplaneAlt, this.airplaneComponent.airplaneDis]);
        break;
      }
      case 'CHANGE': {
        this.airplaneComponent.lightIsPowerOn = !this.airplaneComponent.lightIsPowerOn;
        break;
      }
    }
  }

  validateTakeOff() {
    if (!this.principalComponent.canTakeOff) {
      if (this.airplaneComponent.airplaneDis >= this.airplaneComponent.takeOffDistance) {
        this.principalComponent.canTakeOff = true;
      } else {
        setTimeout(() => {
          this.validateTakeOff();
        }, 1000);
      }
    }
  }

  validateToLand() {
    if (!this.principalComponent.canToLoan) {
      if (this.airplaneComponent.airplaneDis >= this.airplaneComponent.toLandDistance) {
        this.principalComponent.canToLoan = true;
      } else {
        setTimeout(() => {
          this.validateToLand();
        }, 1000);
      }
    }
  }

  isInHome() {
    if (this.airplaneComponent.airplaneDis === this.airplaneComponent.totalDistance - 1) {
      this.principalComponent.canViewReport = true;
    } else {
      setTimeout(() => {
        this.isInHome();
      }, 1000);
    }
  }

}
