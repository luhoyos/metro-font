import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-airplane-view',
  templateUrl: './airplane-view.component.html',
  styleUrls: ['./airplane-view.component.scss']
})
export class AirplaneViewComponent implements OnInit {

  tableViewConfig = [];
  speed = 1;
  maxMSNM = 20
  takeOffDistance = 5;
  toLandDistance = 10;
  totalDistance = 15;

  airplaneAlt = this.maxMSNM - 1;
  airplaneDis = 0;

  lightIsPowerOn = false;

  peopleFalling = [];

  constructor() {
    this.buildTableView();
  }

  ngOnInit() {
    this.fallPeople();
  }

  downAirplane() {
    if (this.airplaneAlt < this.maxMSNM) {
      this.airplaneAlt += 1;
    } else {
      Swal.fire({
        title: '¡Casi chocamos!',
        text: 'Ten cuidado, sabes lo horrible que es morir en un avión',
        icon: 'warning',
        confirmButtonText: '¡Casí casí!'
      }).then(
        () => {
        }
      )
    }
  }

  upAirplane() {
    if (this.airplaneAlt > 0) {
      this.airplaneAlt -= 1;
    } else {
      Swal.fire({
        title: '¡Casi chocamos!',
        text: 'Ten cuidado, sabes lo horrible que es morir en un avión',
        icon: 'warning',
        confirmButtonText: '¡Casí casí!'
      }).then(
        () => {
        }
      )
    }
  }

  toAirplane(toAirplaneAlt) {
    if (this.airplaneAlt === toAirplaneAlt) {
      Swal.fire({
        title: 'We are ready',
        text: '¡Nara nara nara nara nii nii!',
        icon: 'success',
        confirmButtonText: 'Cool'
      }).then(
        () => {
        }
      )
    } else if (this.airplaneAlt > toAirplaneAlt) {
      this.upAirplaneTo(toAirplaneAlt);
    } else {
      this.downAirplaneTo(toAirplaneAlt);
    }
  }

  upAirplaneTo(toAirplaneAlt) {
    console.log(toAirplaneAlt);
    console.log(this.airplaneAlt);
    if (this.airplaneAlt == toAirplaneAlt) {
      Swal.fire({
        title: 'We are ready',
        text: '¡Nara nara nara nara nii nii!',
        icon: 'success',
        confirmButtonText: 'Cool'
      }).then(
        () => {
        }
      )
    } else {
      this.airplaneAlt -= 1;
      setTimeout(() => {
        this.upAirplaneTo(toAirplaneAlt);
      }, 1000);
    }
  }

  downAirplaneTo(toAirplaneAlt) {
    if (this.airplaneAlt == toAirplaneAlt) {
      Swal.fire({
        title: 'We are ready',
        text: '¡Nara nara nara nara nii nii!',
        icon: 'success',
        confirmButtonText: 'Cool'
      }).then(
        () => {
        }
      )
    } else {
      this.airplaneAlt += 1;
      setTimeout(() => {
        this.downAirplaneTo(toAirplaneAlt);
      }, 1000);
    }
  }

  moveAirplaneHorizontally() {
    if (this.airplaneDis === this.totalDistance - 1) {
      Swal.fire({
        title: 'Welcome to the Jungle',
        text: '¡Nara nara nara nara nii nii!',
        icon: 'success',
        confirmButtonText: 'Cool'
      }).then(
        () => {
        }
      )
    } else {
      this.tableViewConfig[this.airplaneAlt][this.airplaneDis] = 1;
      this.airplaneDis = this.airplaneDis + this.speed;
      setTimeout(() => {
        this.moveAirplaneHorizontally();
      }, 1000);
    }
  }

  shouldPaintAirplane(posAlt, posDis) {
    if (posAlt === this.airplaneAlt && posDis === this.airplaneDis) {
      return true;
    } else {
      return false;
    }
  }

  buildTableView() {
    const distanceArray = []
    for (let j = 0; j < this.totalDistance; j++) {
      distanceArray.push(0)
    }
    for (let i = 0; i < this.maxMSNM; i++) {
      this.tableViewConfig.push([...distanceArray]);
    }
  }

  isPeopleFallingHere(posAlt, posDis) {
    let hasPeopleHere = false;
    this.peopleFalling.forEach(
      (personFall) => {
        if (personFall[0] === posAlt && personFall[1] === posDis) {
          hasPeopleHere = true;
        }
      }
    )
    return hasPeopleHere;
  }

  fallPeople() {
    const indexOfPeopleToRemove = []
    for (let i = 0; i < this.peopleFalling.length; i++) {
      if (this.peopleFalling[i][0] === this.maxMSNM - 1) {
        indexOfPeopleToRemove.push(i);
      } else {
        this.peopleFalling[i][0] += 1;
      }
    }

    indexOfPeopleToRemove.forEach(element => {
      this.peopleFalling.splice(element, 1);
    });

    setTimeout(() => {
      this.fallPeople();
    }, 2000);
  }

  toLand() {
    if (this.airplaneAlt !== this.maxMSNM - 1) {
      this.airplaneAlt += 1;
      setTimeout(() => {
        this.toLand()
      }, 1000);
    }
  }

}
