import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AirplaneViewComponent } from './airplane-view.component';

describe('AirplaneViewComponent', () => {
  let component: AirplaneViewComponent;
  let fixture: ComponentFixture<AirplaneViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AirplaneViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AirplaneViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
