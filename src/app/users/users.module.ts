import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedComponentsModule } from './../shared-components/shared-components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { PrincipalViewComponent } from './principal-view/principal-view.component';
import { FilterUsersComponent } from './filter-users/filter-users.component';

@NgModule({
  declarations: [PrincipalViewComponent, FilterUsersComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    SharedComponentsModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class UsersModule { }
