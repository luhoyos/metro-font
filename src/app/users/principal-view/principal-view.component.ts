import { ListUsersComponent } from './../../shared-components/list-users/list-users.component';
import { FilterUsersModel } from './../../../models/searching/filter-users-model';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-principal-view',
  templateUrl: './principal-view.component.html',
  styleUrls: ['./principal-view.component.scss']
})
export class PrincipalViewComponent implements OnInit {

  @ViewChild(ListUsersComponent, null) listUserComponent: ListUsersComponent;

  constructor() { }

  ngOnInit() {
  }

  refreshListWithFilter(filterUserInfo: FilterUsersModel) {
    this.listUserComponent.listWithFilter(filterUserInfo);
  }

}
