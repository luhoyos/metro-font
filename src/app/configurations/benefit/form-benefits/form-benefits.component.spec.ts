import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormbenefitsComponent } from './form-benefits.component';


describe('FormBenefitsComponent', () => {
  let component: FormbenefitsComponent;
  let fixture: ComponentFixture<FormbenefitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormbenefitsComponent ]

    })
    .compileComponents();
  }));

  beforeEach(() => {

    fixture = TestBed.createComponent(FormbenefitsComponent);

    fixture = TestBed.createComponent(FormbenefitsComponent);

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
