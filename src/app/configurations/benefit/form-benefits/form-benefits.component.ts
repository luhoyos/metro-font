import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BackendServiceService } from 'app/shared/backend-service/backend-service.service';
import Swal from 'sweetalert2'
import { RatingModel } from 'models/entities/rating-model';
import { BenefitModel } from 'models/entities/benefit-model';
import { ComunicationModel } from 'models/utils/comunication-model';
import { benefit, rating } from 'constants/api-constants';
import { GenericResponseModel } from 'models/response/generic-response-model';

@Component({
  selector: 'app-form-benefits',
  templateUrl: './form-benefits.component.html',
  styleUrls: ['./form-benefits.component.scss']
})
export class FormbenefitsComponent implements OnInit {
  ratings: Array<RatingModel> = new Array();

  benefitForm: FormGroup;
  @Input() benefitModelInfo: BenefitModel = new BenefitModel();

  @Input() comunicationModelInfo: ComunicationModel = new ComunicationModel();

  @Output() eventEmitter: EventEmitter<any> =  new EventEmitter();

  constructor(
    private formBuilder: FormBuilder,
    private ngbActiveModal: NgbActiveModal,
    private backendService: BackendServiceService
  ) { }

  ngOnInit() {
    this.buildSelect();
    this.buildForm();
  }

  buildSelect() {
    this.backendService.getService(rating.apiGet, {}).then(
      (getRatingResult: any) => {
        this.ratings = [...getRatingResult.listaRespuesta];
      }
    ).catch(
      (getRatingError) => {
        console.error(getRatingError);
      }
    )
  }

  buildForm() {
    if (!(this.comunicationModelInfo.mode === 'view')) {
      this.benefitForm = this.formBuilder.group({
        benefitName: ['', [Validators.required, Validators.maxLength(100)]],
        benefitDescription: ['', [Validators.required, Validators.maxLength(300)]],
        benefitTarifa: ['', [Validators.required, Validators.maxLength(10)]]
      });
    } else {
      this.benefitForm = this.formBuilder.group({
        benefitName: ['', [Validators.required, Validators.maxLength(100)]],
        benefitDescription: ['', [Validators.required, Validators.maxLength(300)]],
        benefitTarifa: ['', [Validators.required, Validators.maxLength(10)]]
      });
    }
  }

  onSubmit() {
    this.backendService.postService(benefit.apiSave, {}, this.benefitModelInfo).then(
      (createBenefitResponse: GenericResponseModel) => {
        if (createBenefitResponse.respuesta === 'EXITO') {
          Swal.fire({
            title: 'Super tu servicio',
            text: '¡Que efectividad!',
            icon: 'success',
            confirmButtonText: 'Cool'
          }).then(
            () => {
              this.eventEmitter.emit('Cool');
            }
          )
        }
      }
    ).catch(
      (createBenefitError) => {
        console.error('GAS TU SERVICIO');
      }
    )
  }

  close() {
    this.ngbActiveModal.close();
  }

  get validatorForm() { return this.benefitForm.controls; }

}
