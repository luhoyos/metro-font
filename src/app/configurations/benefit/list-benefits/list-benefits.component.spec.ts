import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ListbenefitsComponent } from './list-benefits.component';

describe('ListBenefitsComponent', () => {
  let component: ListbenefitsComponent;
  let fixture: ComponentFixture<ListbenefitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListbenefitsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListbenefitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
