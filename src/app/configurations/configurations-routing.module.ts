import { FormRatingsComponent } from './rating/form-ratings/form-ratings.component';
import { ListRatingsComponent } from './rating/list-ratings/list-ratings.component';
import { FormbenefitsComponent } from './benefits/form-benefits/form-benefits.component';
import { ListbenefitsComponent } from './benefits/list-benefits/list-benefits.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListVehiclesComponent } from './vehicles/list-vehicles/list-vehicles.component';



const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'listar-tarifas',
        component: ListRatingsComponent
      },
      {
        path: 'listar-beneficios',
        component: ListbenefitsComponent //listar beneficios 
      },
      {
        path: 'listar-vehiculos',
        component: ListVehiclesComponent //listar beneficios 
      },
      {
        path: 'listar-vehiculos',
        component: ListVehiclesComponent
      },
      {
        path: 'crear-tarifas',
        component: FormRatingsComponent
      },
      {
        path: 'crear-beneficios',
        component: FormbenefitsComponent  //crear beneficios
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationsRoutingModule { }
