import { GenericResponseModel } from './../../../../models/response/generic-response-model';
import { ComunicationModel } from './../../../../models/utils/comunication-model';
import { RatingModel } from './../../../../models/entities/rating-model';
import { BackendServiceService } from './../../../shared/backend-service/backend-service.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2'
import { rating } from 'constants/api-constants';

@Component({
  selector: 'app-form-ratings',
  templateUrl: './form-ratings.component.html',
  styleUrls: ['./form-ratings.component.scss']
})
export class FormRatingsComponent implements OnInit {

  ratingForm: FormGroup;
  @Input() ratingModelInfo: RatingModel = new RatingModel();
  @Input() comunicationModelInfo: ComunicationModel = new ComunicationModel();

  @Output() eventEmitter: EventEmitter<any> = new EventEmitter();

  constructor(
    private formBuilder: FormBuilder,
    private ngbActiveModal: NgbActiveModal,
    private backendService: BackendServiceService
  ) { }

  ngOnInit() {
    this.buildForm();

  }

  buildForm() {
    if (!(this.comunicationModelInfo.mode === 'view')) {
      this.ratingForm = this.formBuilder.group({
        ratingValue: ['', [Validators.required, Validators.max(100000), Validators.maxLength(10)]]
      });
    } else {
      this.ratingForm = this.formBuilder.group({
        ratingValue: ['', []]
      });
    }
  }

  onSubmit() {
    this.backendService.postService(rating.apiSave, {}, this.ratingModelInfo).then(
      (createRaitingResponse: GenericResponseModel) => {
        if (createRaitingResponse.respuesta === 'EXITO') {
          Swal.fire({
            title: 'Super tu servicio',
            text: '¡Que efectividad!',
            icon: 'success',
            confirmButtonText: 'Cool'
          }).then(
            () => {
              this.eventEmitter.emit('Cool');
            }
          )
        }
      }
    ).catch(
      (createRaitingError) => {
        console.error('GAS TU ng ');
      }
    )
  }

  close() {
    this.ngbActiveModal.close();
  }

  get validatorForm() { return this.ratingForm.controls; }

}
