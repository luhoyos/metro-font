import { Component, OnInit } from '@angular/core';
import { BackendServiceService } from 'app/shared/backend-service/backend-service.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormVehiclesComponent } from './../form-vehicles/form.vehicles.component';
import Swal from 'sweetalert2';


@Component({
    selector: 'app-list-vehicles',
    templateUrl: './list-vehicles.component.html',
    styleUrls: ['./list-vehicles.component.scss']
})

export class ListVehiclesComponent implements OnInit {

    vehicles: Array<any> = new Array();
    columns = [
        { prop: 'id' },
        { prop: 'capacidad' },
        { prop: 'nombre' },
        { prop: 'idTipoVehiculo' },
        { prop: 'opciones' }
    ];

    constructor(
        private backendService: BackendServiceService,
        private ngModal: NgbModal
    ) {
    }

    ngOnInit() {
        this.getVehicles();

    }

    getVehicles() {
        this.backendService.getService('vehiculo/api/listarVehiculo', {}).then(
            (getVehiclesResult: any) => {
                this.vehicles = [...getVehiclesResult.listaRespuesta];
            }
        ).catch(
            (getVehiclesgError) => {
                console.error(getVehiclesgError);
            }
        )
    }

    viewVehicles(vehiclesToView: any) {

        const modalOpened = this.ngModal.open(FormVehiclesComponent, {
            backdrop: 'static',
            size: 'lg'
        });
        modalOpened.componentInstance.vehiclesModelInfor = { ...vehiclesToView };
        modalOpened.componentInstance.whatIMDOing = 'view';
    }

    editVehicles(vehiclesToView: any) {
        const modalOpened = this.ngModal.open(FormVehiclesComponent, {
            backdrop: 'static',
            size: 'lg'
        });
        modalOpened.componentInstance.vehiclesModelInfor = { ...vehiclesToView };
        modalOpened.componentInstance.whatIMDOing = 'edit';
        modalOpened.componentInstance.eventEmitter.subscribe(
            (response) => {
                if (response === 'Cool') {
                    this.getVehicles();
                    modalOpened.dismiss();
                }
            }
        )
    }

    createVehicles() {
        const modalOpened = this.ngModal.open(FormVehiclesComponent, {
            backdrop: 'static',
            size: 'lg'
        });
        modalOpened.componentInstance.whatIMDOing = 'create';
        modalOpened.componentInstance.eventEmitter.subscribe(
            (response) => {
                if (response === 'Cool') {
                    this.getVehicles();
                    modalOpened.dismiss();
                }
            }
        )
    }
    deleteVehicles(idToDelete: any) {
        this.backendService.deleteService('vehiculo/api/borrar', {
            idVehiclesABorrar: idToDelete
        }).then(
            (deleteVehiclesResult: any) => {
                if (deleteVehiclesResult.response === 'EXITO') {
                    Swal.fire({
                        title: 'Super tu servicio',
                        text: '¡Que efectividad!',
                        icon: 'success',
                        confirmButtonText: 'Cool'
                    }).then(
                        () => {
                            this.getVehicles();
                        }
                    )
                }
            }
        ).catch(
            (deleteVehiclestError) => {
                console.error(deleteVehiclestError);
            }
        )
    }





}

