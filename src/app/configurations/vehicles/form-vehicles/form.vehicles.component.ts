import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BackendServiceService } from 'app/shared/backend-service/backend-service.service';
import Swal from 'sweetalert2';


@Component({
    selector: 'app-form-vehicles',
    templateUrl: './form-vehicles.component.html',
    styleUrls: ['./form-vehicles.component.scss']
})


export class FormVehiclesComponent implements OnInit {

    vehiclesForm: FormGroup;
    selected = 1;
    tipoVehiculos: any = []

    @Input() vehiclesModelInfo = {

        id: null,
        capacidad: null,
        nombre: null,
        idTipoVehiculo: {
            id: null,
            dato: null
        }


    }

    @Input() whatIMDOing = '';
    @Output() eventEmitter: EventEmitter<any> = new EventEmitter();


    constructor(
        private formBuilder: FormBuilder,
        private ngbActiveModal: NgbActiveModal,
        private backendService: BackendServiceService
    ) { }



    ngOnInit() {
        this.buildForm();
        this.getVehicles();
    }

    buildForm() {
        if (!(this.whatIMDOing === 'view')) {
            this.vehiclesForm = this.formBuilder.group({
                vehiclesCapacidad: ['', [Validators.required, Validators.maxLength(50)]],
                vehiclesNombre: ['', [Validators.required, Validators.maxLength(50)]],
                vehiclesTipoVehiculo: ['', [Validators.required]]
            });
        } else {
            this.vehiclesForm = this.formBuilder.group({
                vehiclesCapacidad: ['', []],
                vehiclesNombre: ['', []],
                vehiclesTipoVehiculo: ['', []]
            });
        }
    }

    onSubmit() {
        this.backendService.postService('vehiculo/api/guardar-o-actualiza',
            {}, this.vehiclesModelInfo).then(
                (createVehiclesResponse: any) => {
                    if (createVehiclesResponse.response === 'EXITO') {
                        Swal.fire({
                            title: 'Super tu servicio',
                            text: '¡Que efectividad!',
                            icon: 'success',
                            confirmButtonText: 'Cool'
                        }).then(
                            () => {
                                this.eventEmitter.emit('Cool');
                            }
                        )
                    }
                }
            ).catch(
                (createBenefitError) => {
                    console.error('GAS TU SERVICIO');
                }
            )
    }
    getVehicles() {
        this.backendService.getService('mv/api/obtener-por-tipo', {
        }).then(
            (getVehiclesResult: any) => {
                this.tipoVehiculos = [...getVehiclesResult.listaRespuesta];
            }
        ).catch(
            (getVehiclesError) => {
                console.error(getVehiclesError);
            }
        )
    }

    close() {
        this.ngbActiveModal.close();
    }

    get validatorForm() { return this.vehiclesForm.controls; }



}
