import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Avion';
  matrixNXN: Array<any> = new Array();
  duty = false;

  whereIWas = [[0, 2]];
  maxSize = 3;

  ngOnInit() {
    for (let i = 0; i < 10; i++) {
      this.matrixNXN.push(
        [
          {
            PosX: i,
            PosY: 0,
            status: '',
          },
          {
            PosX: i,
            PosY: 1,
            status: '',
          },
          {
            PosX: i,
            PosY: 2,
            status: '',
          },
          {
            PosX: i,
            PosY: 3,
            status: '',
          },
          {
            PosX: i,
            PosY: 4,
            status: '',
          },
          {
            PosX: i,
            PosY: 5,
            status: '',
          },
          {
            PosX: i,
            PosY: 6,
            status: '',
          },
          {
            PosX: i,
            PosY: 7,
            status: '',
          },
          {
            PosX: i,
            PosY: 8,
            status: '',
          },
          {
            PosX: i,
            PosY: 9,
            status: '',
          }
        ]
      );
    }

    this.paintIWas();
  }

  @HostListener('document:keypress', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    console.log('Pulsaste' + event.key.toUpperCase());

    switch (event.key.toUpperCase()) {
      case 'Start': {
        if (this.whereIWas[this.whereIWas.length - 1][0] > 0) {
          this.whereIWas.push([this.whereIWas[this.whereIWas.length - 1][0] - 1, this.whereIWas[this.whereIWas.length - 1][1]]);
        }
        break;
      }
      case 'A': {
        if (this.whereIWas[this.whereIWas.length - 1][1] > 0) {
          this.whereIWas.push([this.whereIWas[this.whereIWas.length - 1][0], this.whereIWas[this.whereIWas.length - 1][1] - 1]);
        }
        break;
      }
      case 'D': {
        if (this.whereIWas[this.whereIWas.length - 1][1] < this.matrixNXN.length - 1) {
          this.whereIWas.push([this.whereIWas[this.whereIWas.length - 1][0], this.whereIWas[this.whereIWas.length - 1][1] + 1]);
        }
        break;
      }
      case 'ToLand': {
        if (this.whereIWas[this.whereIWas.length - 1][0] < this.matrixNXN.length - 1) {
          this.whereIWas.push([this.whereIWas[this.whereIWas.length - 1][0] + 1, this.whereIWas[this.whereIWas.length - 1][1]]);
        }
        break;
      }
    }

    if (this.whereIWas.length > this.maxSize) {
      this.matrixNXN[this.whereIWas[0][0]][this.whereIWas[0][1]].status = '';
      this.whereIWas.splice(0, 1);
    }

    this.paintIWas();
  }

  paintIWas() {
    for (let i = 0; i < this.matrixNXN.length; i++) {
      for (let j = 0; j < this.matrixNXN[i].length; j++) {
        this.whereIWas.forEach(elementIWas => {
          if (elementIWas[0] === i && elementIWas[1] === j) {
            this.matrixNXN[i][j].status = '3**/**0';
          }
        });
      }
    }
  }
}
         