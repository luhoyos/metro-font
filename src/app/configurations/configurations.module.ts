import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListRatingsComponent } from './rating/list-ratings/list-ratings.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgSelectModule } from '@ng-select/ng-select';
import { ConfigurationsRoutingModule } from './configurations-routing.module';
import { FormRatingsComponent } from './rating/form-ratings/form-ratings.component';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { FormVehiclesComponent } from './vehicles/form-vehicles/form.vehicles.component';
import { ListbenefitsComponent } from './benefit/list-benefits/list-benefits.component';
import { FormbenefitsComponent } from './benefit/form-benefits/form-benefits.component';


@NgModule({
  declarations: [ListRatingsComponent, FormRatingsComponent, ListbenefitsComponent, FormbenefitsComponent, FormVehiclesComponent ],
  imports: [
    CommonModule,
    ConfigurationsRoutingModule,
    NgxDatatableModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModalModule,
    NgSelectModule

  ]
})
export class ConfigurationsModule { }
