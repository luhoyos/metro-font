import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ValueMasterModel } from 'models/entities/value-master';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { VehicleModel } from 'models/entities/vehicle-model';
import { ComunicationModel } from 'models/utils/comunication-model';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BackendServiceService } from 'app/shared/backend-service/backend-service.service';
import { valueMaster, vehicle } from 'constants/api-constants';
import Swal from 'sweetalert2';
import { GenericResponseModel } from 'models/response/generic-response-model';

@Component({
  selector: 'app-form-vehicles',
  templateUrl: './form-vehicles.component.html',
  styleUrls: ['./form-vehicles.component.scss']
})
export class FormVehiclesComponent implements OnInit {

  typeVehicles: Array<ValueMasterModel> = new Array();

  vehicleForm: FormGroup;

  @Input() vehicleModelInfo: VehicleModel = new VehicleModel();

  @Input() comunicationModelInfo: ComunicationModel = new ComunicationModel();

  @Output() eventEmitter: EventEmitter<any> = new EventEmitter();

  constructor(
    private formBuilder: FormBuilder,
    private ngbActiveModal: NgbActiveModal,
    private backendService: BackendServiceService
  ) { }

  ngOnInit() {
    this.buildSelect();
    this.buildForm();
  }

  buildSelect() {
    this.backendService.getService(valueMaster.apiGetByType,
      {
      'idTipoMaestroValor': 5
      }
       )
       .then((getValueMasterResult: any) => {
         this.typeVehicles = [...getValueMasterResult.listaRespuesta];
         }
       ).catch(
         (getValueMasterError) => {
           console.error(getValueMasterError);
         }
       )
  }

  buildForm() {
    if (!(this.comunicationModelInfo.mode === 'view')) {
      this.vehicleForm = this.formBuilder.group({
        vehicleCapacity: ['', [Validators.required, Validators.maxLength(10)]],
        vehicleName: ['', [Validators.required, Validators.maxLength(100)]],
        tipoVehicle: ['', [Validators.required, Validators.maxLength(10)]]
      });
    } else {
      this.vehicleForm = this.formBuilder.group({
        vehicleCapacity: ['', [Validators.required, Validators.maxLength(10)]],
        vehicleName: ['', [Validators.required, Validators.maxLength(100)]],
        tipoVehicle: ['', [Validators.required, Validators.maxLength(10)]]
      });
    }
  }

  onSubmit() {
    this.backendService.postService(vehicle.apiSave, {}, this.vehicleModelInfo).then(
      (createVehicleResponse: GenericResponseModel) => {
        if (createVehicleResponse.respuesta === 'EXITO') {
          Swal.fire({
            title: 'Super tu servicio',
            text: '¡Que efectividad!',
            icon: 'success',
            confirmButtonText: 'Cool'
          }).then(
            () => {
              this.eventEmitter.emit('Cool');
            }
          )
        }
      }
    ).catch(
      (createVehicleError) => {
        console.error('GAS TU SERVICIO');
      }
    )
  }

  close() {
    this.ngbActiveModal.close();
  }

  get validatorForm() { return this.vehicleForm.controls; }

}
