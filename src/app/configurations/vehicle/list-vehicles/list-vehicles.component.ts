import { Component, OnInit } from '@angular/core';
import { VehicleModel } from 'models/entities/vehicle-model';
import { BackendServiceService } from 'app/shared/backend-service/backend-service.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { vehicle } from 'constants/api-constants';
import { FormVehiclesComponent } from '../form-vehicles/form-vehicles.component';
import { ComunicationModel } from 'models/utils/comunication-model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-vehicles',
  templateUrl: './list-vehicles.component.html',
  styleUrls: ['./list-vehicles.component.scss']
})
export class ListVehiclesComponent implements OnInit {

  vehicles: Array<VehicleModel> = new Array();

  colums = [
    {prop: 'id'},
    {prop: 'capacidad'},
    {prop: 'nombre'},
    {prop: 'idTipoVehiculo'}
  ];

  constructor(
    private backendService: BackendServiceService,
    private ngModal: NgbModal
  ) { }

  ngOnInit() {
    this.getVehicles();
  }

  getVehicles() {
    this.backendService.getService(vehicle.apiGet, { })
    .then(
      (getVehicleResult: any) => {
      this.vehicles = [ ...getVehicleResult.listaRespuesta];
      }
    ).catch(
      (getVehicleError) => {
        console.error(getVehicleError);
      }
    )
  }

  viewVehicle(vehicleToView) {
    const modalOpened = this.ngModal.open(FormVehiclesComponent, {
      backdrop: 'static',
      size: 'lg'
    });
    modalOpened.componentInstance.vehicleModelInfo = {...vehicleToView};
    const comunicationModelInfo: ComunicationModel = new ComunicationModel();
    comunicationModelInfo.mode = 'view';
    modalOpened.componentInstance.comunicationModelInfo = comunicationModelInfo;
  }

  editVehicle(vehicleToView) {
    const modalOpened = this.ngModal.open(FormVehiclesComponent, {
      backdrop: 'static',
      size: 'lg'
    });
    modalOpened.componentInstance.vehicleModelInfo = {...vehicleToView};
    const comunicationModelInfo: ComunicationModel = new ComunicationModel();
    comunicationModelInfo.mode = 'edit';
    modalOpened.componentInstance.comunicationModelInfo = comunicationModelInfo;
    modalOpened.componentInstance.eventEmitter.subscribe(
      (response) => {
        if (response === 'Cool') {
          this.getVehicles();
          modalOpened.dismiss();
        }
      }
    )
  }

  createVehicle() {
    const modalOpened = this.ngModal.open(FormVehiclesComponent, {
      backdrop: 'static',
      size: 'lg'
    });
    const comunicationModelInfo: ComunicationModel = new ComunicationModel();
    comunicationModelInfo.mode = 'create';
    modalOpened.componentInstance.comunicationModelInfo = comunicationModelInfo;
    modalOpened.componentInstance.eventEmitter.subscribe(
      (response) => {
        if (response === 'Cool') {
          this.getVehicles();
          modalOpened.dismiss();
        }
      }
    )
  }

  deleteVehicle(idToDelete) {
    this.backendService.deleteService(vehicle.apiDelete, {
      idVehiculoABorrar: idToDelete
    }).then(
      (deleteVehicleResult: any) => {
        if (deleteVehicleResult.respuesta === 'EXITO') {
          Swal.fire({
            title: 'Super tu servicio',
            text: '¡Que efectividad!',
            icon: 'success',
            confirmButtonText: 'Cool'
          }).then(
            () => {
              this.getVehicles();
            }
          )
        }
      }
    ).catch(
      (deleteVehicleError) => {
        console.error(deleteVehicleError);
      }
    )
  }

}
